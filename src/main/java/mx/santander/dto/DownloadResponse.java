package mx.santander.dto;

import java.io.File;
import java.io.Serializable;

import org.springframework.core.io.InputStreamResource;

public class DownloadResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private File file;
	private InputStreamResource resource;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public InputStreamResource getResource() {
		return resource;
	}

	public void setResource(InputStreamResource resource) {
		this.resource = resource;
	}

}
