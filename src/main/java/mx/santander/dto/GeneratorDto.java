package mx.santander.dto;

public class GeneratorDto {

    private String api_design;
    private String api_version;
    private String api_domain;

    public String getApi_design() {
        return api_design;
    }

    public void setApi_design(String api_design) {
        this.api_design = api_design;
    }

    public String getApi_version() {
        return api_version;
    }

    public void setApi_version(String api_version) {
        this.api_version = api_version;
    }

    public String getApi_domain() {
        return api_domain;
    }

    public void setApi_domain(String api_domain) {
        this.api_domain = api_domain;
    }
}
