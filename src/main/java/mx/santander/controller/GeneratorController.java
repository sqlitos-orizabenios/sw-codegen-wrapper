package mx.santander.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import mx.santander.dto.GeneratorResponse;
import mx.santander.model.GeneratorRequest;
import mx.santander.service.GeneratorService;

/**
 * TODO Controller donde ejecutamos el generador de models y descargar el
 * archivo zip
 * 
 * @author José Luis Rojas
 *
 */
@RestController
@RequestMapping(value = "/v1/generetors", produces = { "application/json" })
public class GeneratorController {

	@Autowired
	private GeneratorService service;

	@ApiOperation(value = "Return the string", produces = "application/json", response = String.class, httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 404, message = "String Not found "),
			@ApiResponse(code = 500, message = "Internal Server error") })
	@PostMapping("/models")
	@ResponseBody
	public GeneratorResponse getGenerator(@RequestBody GeneratorRequest generatorRequest) throws IOException {

		return service.getGenerator(generatorRequest);
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> download() throws IOException {

		return service.getDownloadZip();

	}
}
