package mx.santander.model;

import java.io.Serializable;

/**
 * TODO El request de entrada generando sus POJO's
 * 
 * @author Aaron Jazhiel Delgado Gonzalez
 * @author Z061798
 */
public class GeneratorRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String apiURL;
	private String language;

	public String getApiURL() {
		return apiURL;
	}

	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
