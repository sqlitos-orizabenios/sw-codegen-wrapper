package mx.santander.config;

import io.airlift.airline.Command;
import io.airlift.airline.Option;
import io.swagger.parser.SwaggerParser;
import io.swagger.parser.util.SwaggerDeserializationResult;
import mx.santander.exceptions.ValidateException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * TODO
 * Realiza la validación de la url donde buscamos el archivo swagger
 * @author José Luis Rojas Pérez
 *
 */
@Command(name = "validate", description = "Validate specification")
public class Validate implements Runnable {

    @Option(name = {"-i", "--input-spec"}, title = "spec file", required = true,
            description = "location of the swagger spec, as URL or file (required)")
    private String spec;

    @Override
    public void run() {
      

        SwaggerParser parser = new SwaggerParser();
        SwaggerDeserializationResult result = parser.readWithInfo(spec, null, true);
        List<String> messageList = result.getMessages();
        Set<String> messages = new HashSet<String>(messageList);

   

        if (messages.size() > 0) {
            throw new ValidateException();
        }
    }
}
