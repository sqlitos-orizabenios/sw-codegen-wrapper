package mx.santander.config;

import ch.lambdaj.collection.LambdaIterable;
import io.airlift.airline.Command;
import io.swagger.codegen.CodegenConfig;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.collection.LambdaCollections.with;
import static java.util.ServiceLoader.load;

/**
 * TODO
 * Identifica que el lenguaje a ejecutar sea compatible
 * @author José Luis Rojas
 * 
 */
@Command(name = "langs", description = "Shows available langs")
public class Langs implements Runnable {
    @Override
    public void run() {
        LambdaIterable<String> langs =
                with(load(CodegenConfig.class)).extract(on(CodegenConfig.class).getName());
    
    }
}
