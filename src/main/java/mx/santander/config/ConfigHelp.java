package mx.santander.config;

import io.airlift.airline.Command;
import io.airlift.airline.Option;
import io.swagger.codegen.CodegenConfig;
import io.swagger.codegen.CodegenConfigLoader;

/**
 * TODO
 * Clase para ejecutar el swagger codegen haciendo la descripción a que lenguaje generar el arquetipo
 * @author José Luis Rojas Pérez
 *
 */

@Command(name = "config-help", description = "Config help for chosen lang")
public class ConfigHelp implements Runnable {

    @Option(name = {"-l", "--lang"}, title = "language", required = true,
            description = "language to get config help for")
    private String lang;

    @Override
    public void run() {
  
        CodegenConfig config = CodegenConfigLoader.forName(lang);
   
    }
}
