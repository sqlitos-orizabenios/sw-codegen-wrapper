package mx.santander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO
 * Clase donde ejecuta el springboot y levanta el pryecto
 * @author José Luis Rojas
 *
 */
@SpringBootApplication
public class Generator {

	public static void main(String[] args) {
		SpringApplication.run(Generator.class, args);
	}

}
