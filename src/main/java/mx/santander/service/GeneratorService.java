package mx.santander.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.airlift.airline.Cli;
import io.airlift.airline.Help;
import mx.santander.config.ConfigHelp;
import mx.santander.config.Generate;
import mx.santander.config.Langs;
import mx.santander.config.Meta;
import mx.santander.config.Validate;
import mx.santander.config.Version;
import mx.santander.config.YAMLConfig;
import mx.santander.controller.GeneratorController;
import mx.santander.dto.DownloadResponse;
import mx.santander.dto.GeneratorResponse;
import mx.santander.model.GeneratorRequest;

/**
 * 
 * @author Jose Luis Rojas
 * @version Realiza el arquetipo y despues comprime el directorio
 *
 */
@Service
public class GeneratorService {

	@Autowired
	private YAMLConfig myConfig;

	/**
	 * Parametros de configuración
	 * 
	 * @param myConfig
	 */
//	@Autowired
//	public void setMyConfig(YAMLConfig myConfig) {
//		this.myConfig = myConfig;
//	}

	private static final Logger logger = LoggerFactory.getLogger(GeneratorController.class);

	/**
	 * Generador del arquetipo, comprime el directorio y posteriormente lo elimina,
	 * dejando solo el archivo zip
	 * 
	 * @param generatorRequest
	 * @return GeneratorResponse
	 * @throws IOException
	 */
	public GeneratorResponse getGenerator(GeneratorRequest generatorRequest) throws IOException {

		GeneratorResponse response = new GeneratorResponse();

		// TODO Genera el arquetipo con la libreria swagger-codegen
		String version = Version.readVersionFromResources();
		@SuppressWarnings("unchecked")

		Cli.CliBuilder<Runnable> builder = Cli.<Runnable>builder("swagger-codegen-cli")
				.withDescription(
						String.format("Swagger code generator CLI (version %s). More info on swagger.io", version))
				.withDefaultCommand(Langs.class).withCommands(Generate.class, Meta.class, Langs.class, Help.class,
						ConfigHelp.class, Validate.class, Version.class);

		// TODO Genera la cadena con la URL, nombre del package, y folder destino
		String cadena[] = { "generate", "-i", generatorRequest.getApiURL(), "-l", generatorRequest.getLanguage(),
				"--model-package", myConfig.getPackages(), "-Dmodels", "-DsupportingFiles=pom.xml", "-o",
				myConfig.getResources() };

		builder.build().parse(cadena).run();

		// TODO Utileria para comprimir los archivos
		ZipUtils.zipIt(myConfig.getResources(), myConfig.getZipFile());

		response.setDescription("Generation of models");
		response.setApiURL(generatorRequest.getApiURL());
		response.setStatus("Success");
		response.setLanguage(generatorRequest.getLanguage());
		response.setZipFile(myConfig.getZipFile().substring(21));

		// TODO Borramos el directorio
		DeleteDirectory.deleteValidaPath(myConfig.getResources());

		logger.info("Generación de Modelo " + myConfig.getZipFile());

		return response;

	}

	/**
	 * Clase donde descarga el archivo zip
	 * 
	 * @param models
	 * @return response
	 * @throws FileNotFoundException
	 */
	public ResponseEntity<InputStreamResource> getDownloadZip() throws FileNotFoundException {

		File file = new File(myConfig.getZipFile());

		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

		DownloadResponse response = new DownloadResponse();
		response.setFile(file);
		response.setResource(resource);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + response.getFile().getName())
				.contentType(MediaType.APPLICATION_PDF).contentLength(response.getFile().length())
				.body(response.getResource());

	}

}
