package mx.santander.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.santander.controller.GeneratorController;

/**
 * Utileria para comprimir el directorio
 * 
 * @author José Luis Rojas
 *
 */
public class ZipUtils {

	private static final Logger logger = LoggerFactory.getLogger(GeneratorController.class);

	public static void zipIt(String sourceDirPath, String zipFilePath) throws IOException {

		Path validatePath = Paths.get(zipFilePath);

		if (Files.exists(validatePath) == true) {

			DeleteDirectory.deleteValidaPath(zipFilePath);

		}

		Path p = Files.createFile(Paths.get(zipFilePath));
		Path pp = Paths.get(sourceDirPath);

		try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p)); Stream<Path> paths = Files.walk(pp)) {
			paths.filter(path -> !Files.isDirectory(path)).forEach(path -> {

				ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());

				try {
					zs.putNextEntry(zipEntry);
					Files.copy(path, zs);
					zs.closeEntry();

					logger.info("Generación del archivo models.zip");

				} catch (IOException e) {
					logger.error("Error al generar zip " + e);
				}

			});

		}

	}
}