package mx.santander.service;

import java.io.File;

/**
 * Utileria para borrar directorio
 * @autor Jose Luis Rojas
 */

public class DeleteDirectory {

	public static boolean deleteValidaPath(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return true;
		}
		if (!file.isDirectory()) {
			return file.delete();
		}
		return DeleteDirectory.deleteDirectory(file) && file.delete();
	}

	private static boolean deleteDirectory(File dir) {
		File[] children = dir.listFiles();
		boolean childrenDeleted = true;
		for (int i = 0; children != null && i < children.length; i++) {
			File child = children[i];
			if (child.isDirectory()) {
				childrenDeleted = deleteDirectory(child) && childrenDeleted;
			}
			if (child.exists()) {
				childrenDeleted = child.delete() && childrenDeleted;
			}
		}
		return childrenDeleted;
	}

}
